package no.bacon;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;
import javax.swing.text.DefaultCaret;

/**
 * Controls everything that is menu related such as displaying the menu, popup windows, and its events.
 * <p>
 * @author      HIG Studenter <121232 @ hig.no>
 * @version     13.37
 * @since       2014-10-17
 * @param table is the table that is generated in the GBEditor
 * @param tableModel is the tableModel that is generated in the GBEditor
 * @param messages is the ResourceBundle which is received from GBEditor, which contains the proper strings
 * to display depending on which country and language the user is from/is using.
 * @param mainComponent is the GBEditor, it is used to get the frame which everything is printed to.
 * @param openedFileName is the currently opened file path. If there is no file saved or loaded, this is
 * equal to "", an empty string.
 * @param currentVersion is the current version of the program, if there ever is a change in the way to load
 * or save files, this must be changed in the GBEditor.java file
 * @param fc is the filechooser, a popup window which appears in order for the user to choose a file or
 * directory to save to or load from
 * @param rmbMenu is the popup menu which appears when the user right clicks on a row in the table.
 */

public class MenuHandler {
	static JTable table;
	static TableModel tableModel;
	static ResourceBundle messages;
	GBEditor mainComponent;
	String openedFileName = "";
	String currentVersion = "";
	final JFileChooser fc = new JFileChooser();
	JPopupMenu rmbMenu = new JPopupMenu();
	
	MenuHandler(String currentVersion, GBEditor component, JTable table, ResourceBundle messages) {
		this.currentVersion = currentVersion;
		this.messages = messages;
		mainComponent = component;
		this.table = table;
		
		// ---MENU---
		// _-=AbstractActions for toolbar and menu presses=-_
		AbstractAction newFile = new AbstractAction (messages.getString("newOption"), new ImageIcon (getClass().getResource("New.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				tableModel.resetRows();
				
				
				table = new JTable(tableModel);
				table.getColumnModel().getColumn(0).setMinWidth(50);
				table.getColumnModel().getColumn(1).setMinWidth(100);
				table.getColumnModel().getColumn(2).setMinWidth(50);
				table.getColumnModel().getColumn(3).setMinWidth(50);
				table.getColumnModel().getColumn(4).setMinWidth(50);
				table.getColumnModel().getColumn(5).setMinWidth(50);
				table.getColumnModel().getColumn(6).setMinWidth(60);
				table.getColumnModel().getColumn(7).setMinWidth(50);
				table.getColumnModel().getColumn(7).setPreferredWidth(100);
				table.getColumnModel().getColumn(8).setMinWidth(50);
				table.getColumnModel().getColumn(8).setPreferredWidth(100);
				
				ImageIcon[] fillImages = {
					new ImageIcon(getClass().getResource("skaler_ingen.png"), Integer.toString(GridBagConstraints.NONE)),
					new ImageIcon(getClass().getResource("skaler_horisontalt.png"), Integer.toString(GridBagConstraints.HORIZONTAL)),
					new ImageIcon(getClass().getResource("skaler_vertikalt.png"), Integer.toString(GridBagConstraints.VERTICAL)),
					new ImageIcon(getClass().getResource("skaler_begge.png"), Integer.toString(GridBagConstraints.BOTH))
				};
				
				ImageIcon[] anchorImages = {
					new ImageIcon(getClass().getResource("anchor_center.png"), Integer.toString(GridBagConstraints.CENTER)),
					new ImageIcon(getClass().getResource("anchor_north.png"), Integer.toString(GridBagConstraints.NORTH)),
					new ImageIcon(getClass().getResource("anchor_south.png"), Integer.toString(GridBagConstraints.SOUTH)),
					new ImageIcon(getClass().getResource("anchor_east.png"), Integer.toString(GridBagConstraints.EAST)),
					new ImageIcon(getClass().getResource("anchor_west.png"), Integer.toString(GridBagConstraints.WEST)),
					new ImageIcon(getClass().getResource("anchor_northeast.png"), Integer.toString(GridBagConstraints.NORTHEAST)),
					new ImageIcon(getClass().getResource("anchor_northwest.png"), Integer.toString(GridBagConstraints.NORTHWEST)),
					new ImageIcon(getClass().getResource("anchor_southeast.png"), Integer.toString(GridBagConstraints.SOUTHEAST)),
					new ImageIcon(getClass().getResource("anchor_southwest.png"), Integer.toString(GridBagConstraints.SOUTHWEST)),
					
				};
				
				JComboBox<String> componentTypeSelector = new JComboBox<String>(componentTypeName);
				JComboBox<ImageIcon> fillSelector = new JComboBox<ImageIcon>(fillImages);
				JComboBox<ImageIcon> anchorSelector = new JComboBox<ImageIcon>(anchorImages);
				
				TableColumn typeColumn = table.getColumnModel().getColumn(0);
				TableColumn fillColumn = table.getColumnModel().getColumn(7);
				TableColumn anchorColumn = table.getColumnModel().getColumn(8);
				
				
				typeColumn.setCellEditor(new DefaultCellEditor(componentTypeSelector));
				fillColumn.setCellEditor(new DefaultCellEditor(fillSelector));
				anchorColumn.setCellEditor(new DefaultCellEditor(anchorSelector));
				
				fillColumn.setCellRenderer(new ImageFromListRenderer(fillImages));
				anchorColumn.setCellRenderer(new ImageFromListRenderer(anchorImages));
				
				
				
				
				
				
				
				
				
				
				
				
				tableModel.addRowEmpty();
				openedFileName = "";
			}
		};
		AbstractAction loadFile = new AbstractAction (messages.getString("loadOption"), new ImageIcon (getClass().getResource("OpenDoc.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				loadFile();
			}
		};
		AbstractAction saveFile = new AbstractAction (messages.getString("saveOption"), new ImageIcon (getClass().getResource("Save.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				if (openedFileName.equals("")) {
					saveAsFile();
				} else {
					saveFile();
				}
			}
		};
		AbstractAction preview = new AbstractAction (messages.getString("previewOption"), new ImageIcon (getClass().getResource("ExecuteProject.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				int i = 0;
				for(GuiElement element : tableModel.data){
					System.out.println("ROW: " + i + 
							" - " + element.getComponent() +
							" " + element.getVarName() + 
							" " + element.getText() + 
							" " + element.getRow() + 
							" " + element.getColumn() + 
							" " + element.getRows() + 
							" " + element.getColumns() + 
							" " + element.getFill() + 
							" " + element.getAnchor());
					i++;
				}
			}
		};
		AbstractAction generateCode = new AbstractAction (messages.getString("generateCodeOption"), new ImageIcon (getClass().getResource("SaveJava.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				// If there is something in the table
				if(tableModel.data.size() > 0){
					// Display a file chooser
					fc.setDialogTitle(messages.getString("saveGeneratedDialogTitle"));
					fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    	int returnVal = fc.showSaveDialog(mainComponent);
			    	String generatedFileName;
			    	
			    	// If the dude approves
			    	if (returnVal == JFileChooser.APPROVE_OPTION) {
			    	    File file = fc.getSelectedFile();
			    	    
			    	    // Save the file in the directory specified, with this filename
			    	    generatedFileName = file.getAbsolutePath() + "\\GeneratedLayout.java";
			    	    
			    	} else {
			    		return;
			    	}
			    	// Revert back to standard file chooser state which is used for everything else
			    	fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			    	
			    	// Generate a long string which contains all of the java code to be written
					String generatedCode = "package no.bacon;\n";
					generatedCode += "import java.awt.GridBagConstraints;\n";
					generatedCode += "import java.awt.GridBagLayout;\n";
					generatedCode += "import javax.swing.JButton;\n";
					generatedCode += "import javax.swing.JTextArea;\n";
					generatedCode += "import javax.swing.JTextField;\n";
					generatedCode += "import javax.swing.JFrame;\n";
					generatedCode += "import javax.swing.JLabel;\n";
					generatedCode += "import javax.swing.JPanel;\n";
					
					generatedCode += "public class GeneratedLayout extends JFrame {\n";
					generatedCode += "\tpublic GeneratedLayout(){\n";
					generatedCode += "\t\tJPanel panel = new JPanel();\n";
					generatedCode += "\t\tGridBagConstraints constraints = new GridBagConstraints();\n";
					generatedCode += "\t\tpanel.setLayout(new GridBagLayout());\n\n";
					
					for (GuiElement element : tableModel.data) {
						String newVar = element.getComponent();
						newVar += " " + element.getVarName();
						newVar += " = new " + element.getComponent();
						newVar += "(\"" + element.getText() + "\");\n";
						generatedCode += "\t\t" + newVar;
					}
					
					generatedCode += "\n\n";
					
					for (GuiElement element : tableModel.data) {
						String newConstraints = "";
						newConstraints += "\t\tconstraints.anchor = " + element.getAnchor() + ";\n";
						newConstraints += "\t\tconstraints.fill = " + element.getFill() + ";\n";
						newConstraints += "\t\tconstraints.gridx = " + element.getColumn() + ";\n";
						newConstraints += "\t\tconstraints.gridy = " + element.getRow() + ";\n";
						if(element.getComponent().contains("JText")){
							newConstraints += "\t\tconstraints.gridwidth = " + element.getBreadth() + ";\n";
							newConstraints += "\t\tconstraints.gridheight = " + element.getHeight() + ";\n";
							if(element.getComponent().contains("Area")){
								newConstraints += "\t\t" + element.getVarName() + ".setWrapStyleWord(" + element.getUseWordWrap() + ");\n";
							}
						}
						
						generatedCode += newConstraints;
						generatedCode += "\t\tpanel.add(" + element.getVarName() + ", constraints);\n\n";
					}
					
					generatedCode += "\t\tsetDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n";
					generatedCode += "\t\tadd(panel);\n";
					generatedCode += "\t\tpack();\n";
					generatedCode += "\t\tsetVisible(true);\n";
					generatedCode += "\t}\n";
					generatedCode += "\tpublic static void main(String args[]){\n";
					generatedCode += "\t\tGeneratedLayout program = new GeneratedLayout();\n";
					generatedCode += "\t}\n}";
					
					// And write it to the file specified
					try {
						FileOutputStream fos = new FileOutputStream (generatedFileName);
						OutputStreamWriter osw = new OutputStreamWriter(fos);
						BufferedWriter bw = new BufferedWriter(osw);
						
						bw.write(generatedCode);
						bw.close();
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
					
					
					
				}
			}
		};
		AbstractAction newRow = new AbstractAction (messages.getString("newRowOption"), new ImageIcon (getClass().getResource("NewRow.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				tableModel.addRowEmpty();
			}
		};
		AbstractAction help = new AbstractAction (messages.getString("helpOption"), new ImageIcon (getClass().getResource("Help.gif"))) {
			public void actionPerformed (ActionEvent ae) {
				JOptionPane.showMessageDialog (null, messages.getString("helpOptionText"), messages.getString("helpOption"), JOptionPane.PLAIN_MESSAGE);
			}
		};
		
		// _-=Mnemonic keys & tooltip for toolbar buttons/menu items=-_
		newFile.putValue(AbstractAction.MNEMONIC_KEY, (int)'N');
		newFile.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("newFile"));
		loadFile.putValue(AbstractAction.MNEMONIC_KEY, (int)'L');
		loadFile.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("loadFile"));
		saveFile.putValue(AbstractAction.MNEMONIC_KEY, (int)'S');
		saveFile.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("saveFile"));
		preview.putValue(AbstractAction.MNEMONIC_KEY, (int)'P');
		preview.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("generatePreview"));
		generateCode.putValue(AbstractAction.MNEMONIC_KEY, (int)'G');
		generateCode.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("generateJava"));
		newRow.putValue(AbstractAction.MNEMONIC_KEY, (int)'G');
		newRow.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("newRow"));
		help.putValue(AbstractAction.MNEMONIC_KEY, (int)'H');
		help.putValue(AbstractAction.SHORT_DESCRIPTION, messages.getString("showHelp"));
		
		// _-=Menu handling=-_
		JMenu fileMenu = new JMenu (messages.getString("file"));
		fileMenu.setMnemonic ('F');
		
		JMenuItem newItem = new JMenuItem (newFile);
		newItem.setAccelerator (KeyStroke.getKeyStroke ('N', InputEvent.CTRL_DOWN_MASK));
		fileMenu.add (newItem);
		
		JMenuItem loadItem = new JMenuItem (loadFile);
		loadItem.setAccelerator (KeyStroke.getKeyStroke ('L', InputEvent.CTRL_DOWN_MASK));
		fileMenu.add (loadItem);
		
		JMenuItem saveItem = new JMenuItem (saveFile);
		saveItem.setAccelerator (KeyStroke.getKeyStroke ('S', InputEvent.CTRL_DOWN_MASK));
		fileMenu.add (saveItem);
		
		JMenuItem saveAsItem = new JMenuItem (messages.getString("savingFile"), createImageIcon("Save.gif"));
		saveAsItem.setMnemonic('A');
		fileMenu.add (saveAsItem);
		
		fileMenu.addSeparator();
		
		JMenuItem previewItem = new JMenuItem (preview);
		previewItem.setAccelerator (KeyStroke.getKeyStroke ('P', InputEvent.CTRL_DOWN_MASK));
		fileMenu.add (previewItem);
		
		JMenuItem genJavaItem = new JMenuItem (generateCode);
		genJavaItem.setAccelerator (KeyStroke.getKeyStroke ('G', InputEvent.CTRL_DOWN_MASK));
		fileMenu.add (genJavaItem);
		
		fileMenu.addSeparator();
		
		JMenuItem exitItem = new JMenuItem (messages.getString("exit"));
		exitItem.setMnemonic('E');
		fileMenu.add (exitItem);
		
		JMenu editMenu = new JMenu (messages.getString("edit"));
		editMenu.setMnemonic ('E');
		
		JMenuItem newRowItem = new JMenuItem (newRow);
		newRowItem.setAccelerator (KeyStroke.getKeyStroke ('R', InputEvent.CTRL_DOWN_MASK));
		editMenu.add (newRowItem);
		
		editMenu.addSeparator();
		
		JMenuItem settingsItem = new JMenuItem (messages.getString("settings"));
		settingsItem.setMnemonic('S');
		editMenu.add (settingsItem);
		
		
		JMenu helpMenu = new JMenu (messages.getString("help"));
		helpMenu.setMnemonic ('H');
		
		JMenuItem helpItem = new JMenuItem (help);
		helpItem.setAccelerator (KeyStroke.getKeyStroke (KeyEvent.VK_F1, 0));
		helpMenu.add (helpItem);
		
		helpMenu.addSeparator();
		
		JMenuItem aboutItem = new JMenuItem (messages.getString("aboutOption"));
		aboutItem.setMnemonic('A');
		helpMenu.add (aboutItem);
		
		// Create the actual menu bar
		JMenuBar bar = new JMenuBar ();
		// Setting it as the frames menu bar
		component.setJMenuBar (bar);
		// Adding the file menu to the menu bar
		bar.add (fileMenu);
		bar.add (editMenu);
		bar.add (helpMenu);
		
		// ---TOOLBAR---
		JToolBar toolbar = new JToolBar ();
		toolbar.add(newFile);
		toolbar.add(loadFile);
		toolbar.add(saveFile);
		toolbar.addSeparator();
		
		toolbar.add(preview);
		toolbar.add(generateCode);
		toolbar.addSeparator();
		
		toolbar.add(newRow);
		toolbar.addSeparator();
		
		toolbar.add(help);
		toolbar.addSeparator();
		
		component.add(toolbar, BorderLayout.NORTH);
		
		// _-=Right mouse button popup items=-_
		JMenuItem showSettings = new JMenuItem (messages.getString("rmbShowAttributeSettings"));
		JMenuItem removeObject = new JMenuItem (messages.getString("rmbRemoveSelectedRow"));
		rmbMenu.add(showSettings);
		rmbMenu.add(removeObject);
		
		// ---LISTENERS---
		// Add listener to components that can bring up popup menus.
	    MouseListener popupListener = new PopupListener();
	    table.addMouseListener(popupListener);
		
		aboutItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				JOptionPane.showMessageDialog (null, messages.getString("aboutOptionText"), messages.getString("aboutOption"), JOptionPane.PLAIN_MESSAGE);
			}
		});
		
		saveAsItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				saveAsFile();
			}
		});
		
		// _-=Right mouse button popup specific listeners=-_
		// Open another small window when right mouse button + settings is called
		showSettings.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				int selectedRow = table.getSelectedRow();
				String tmp = (String)tableModel.getValueAt(selectedRow, 0);
				if (tmp.equals("JTextArea")) {
					showJTextAreaSettings();
				} else if (tmp.equals("JTextField")) {
					showJTextFieldSettings();
				}
			}
		});
		
		// Removes the row that the user right clicked on and chose to remove
		removeObject.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				int selectedRow = table.getSelectedRow();
				tableModel.removeRow(selectedRow);
			}
		});
		
	}
	
	/** 
	 * Returns an ImageIcon, or null if the path was invalid.
	 */
    protected ImageIcon createImageIcon(String path) {
        
    	URL imageURL = this.getClass().getResource(path);
    	
        if (imageURL != null) {
            return new ImageIcon(imageURL);
        } else {
            System.err.println(messages.getString("fileNotFound") + ": " + path);
            return null;
        }
    }
	
    /**
     *  Writes the file directly to disk using the filepath from
     *  a previously opened or saved file, or a newly specified path.
     */
    public void saveFile() {
    	try {
			FileOutputStream fos = new FileOutputStream (openedFileName);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osw);
			
			bw.write(currentVersion);
			bw.newLine();
			bw.write(String.valueOf(table.getRowCount()));
			bw.newLine();
			bw.write(String.valueOf(table.getColumnCount()));
			bw.newLine();
			
			for (int i = 0; i < table.getRowCount(); i++) {
				for (int j = 0; j < table.getColumnCount(); j++) {
					// Write strings as strings
					String columnName = tableModel.getColumnName(j);
					if (columnName.equals("Type") || columnName.equals("Variable name") || columnName.equals("Text")) {
						bw.write((String)table.getValueAt(i, j));
					} else {
						// Else convert ints to strings and write them
						bw.write(String.valueOf(table.getValueAt(i, j)));
					}
					bw.newLine();
				}
				// Write additional "hidden" attributes about this row
				bw.write(String.valueOf(tableModel.data.elementAt(i).getBreadth()));
				bw.newLine();
				bw.write(String.valueOf(tableModel.data.elementAt(i).getHeight()));
				bw.newLine();
				bw.write(String.valueOf(tableModel.data.elementAt(i).getUseJScrollPane()));
				bw.newLine();
				bw.write(String.valueOf(tableModel.data.elementAt(i).getUseWordWrap()));
				bw.newLine();
			}
			bw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
    }
    
    /**
     * Opens a file chooser where the user can specify a directory 
     * and a file name to save the file as.
     */
    public void saveAsFile() {
    	fc.setDialogTitle(messages.getString("saveFileDialogTitle"));   
    	 
    	int returnVal = fc.showSaveDialog(mainComponent);
    	 
    	if (returnVal == JFileChooser.APPROVE_OPTION) {
    	    File file = fc.getSelectedFile();
    	    if (!file.getName().endsWith(".gbe")) {
    	    	openedFileName = file.getAbsolutePath() + ".gbe";
    	    } else {
    	    	openedFileName = file.getAbsolutePath();
    	    }
    	    saveFile();
    	}
    	
    }
    
    /**
     * Opens a file chooser for the user to browse after a file to open.
     * Will return early if the user chooses a file with the wrong format.
     * The file version must be supported by the loader.
     */
    public void loadFile() {
    	fc.setDialogTitle(messages.getString("loadFileQuery"));
    	int returnVal = fc.showOpenDialog(mainComponent);
    	

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            
            if (!file.getName().endsWith(".gbe")) {
            	JOptionPane.showMessageDialog (null, messages.getString("invalidFileTypeWarning"), "Error", JOptionPane.PLAIN_MESSAGE);
            	return;
            }
            
            try {
            	FileInputStream fis = new FileInputStream (file.getAbsolutePath());
    			InputStreamReader isr = new InputStreamReader(fis);
    			BufferedReader br = new BufferedReader(isr);
    			
    			String version = br.readLine();
    			if (!version.equals(currentVersion)) {
    				JOptionPane.showMessageDialog (null, messages.getString("invalidFileVersionWarning"), "Error", JOptionPane.PLAIN_MESSAGE);
    				br.close();
    				return;
    			}
    			
    			int rowCount = Integer.parseInt(br.readLine());
    			int columnCount = Integer.parseInt(br.readLine());
    			
    			tableModel.resetRows();
    			for (int i = 0; i < rowCount; i++) {
    				tableModel.addRowEmpty();
    				for (int j = 0; j < columnCount; j++) {
    					table.setValueAt(br.readLine(), i, j);
    				}
    				// Read additional "hidden" attributes about this row
    				tableModel.data.elementAt(i).setBreadth(Integer.parseInt(br.readLine()));
    				tableModel.data.elementAt(i).setHeight(Integer.parseInt(br.readLine()));
    				tableModel.data.elementAt(i).setUseJScrollPane(Boolean.valueOf(br.readLine()));
    				tableModel.data.elementAt(i).setUseWordWrap(Boolean.valueOf(br.readLine()));
    			}
    			openedFileName = file.getAbsolutePath();
    			br.close();
    			
    		} catch (IOException ioe) {
    			ioe.printStackTrace();
    		}
        }
    }

    /**
     * 
     * @author Sindre
     * Handles the popup event, given that a user right clicks on a cell.
     *
     */
    class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
        	showPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
        	showPopup(e);
        }

        private void showPopup(MouseEvent e) {
        	// Get the row that was right clicked on
        	int r = table.rowAtPoint(e.getPoint());
        	// Make that row selected
	        if (r >= 0 && r < table.getRowCount()) {
	            table.setRowSelectionInterval(r, r);
	        } else {
	            table.clearSelection();
	        }

	        int rowIndex = table.getSelectedRow();
	        if (rowIndex < 0) {
	            return;
	        }
	        	
	        // Launch the proper popup window for that row and column
	        if (e.isPopupTrigger() && e.getComponent() instanceof JTable ) {
	        	rmbMenu.show(e.getComponent(), e.getX(), e.getY());
	        }
        }
    }
    
    /**
     * Will display a window with settings, specified for an object
     * of type JTextArea.
     */
    public static void showJTextAreaSettings() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
            	JFrame frame = new JFrame(messages.getString("textAreaSettingsTitle"));
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                try {
                	UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception e) {
                   e.printStackTrace();
                }
                JPanel panel = new JPanel();
                JPanel topPanel = new JPanel();
                JPanel lowPanel = new JPanel();
                
                panel.setLayout(new GridBagLayout());
                topPanel.setLayout(new BorderLayout());
                lowPanel.setLayout(new BorderLayout());
                
                GridBagConstraints c = new GridBagConstraints();
                panel.setOpaque(true);
                JLabel textHint = new JLabel(messages.getString("textAreaSettingsInstruction"));
                JLabel textColumn = new JLabel(messages.getString("columns"));
                JLabel textRow = new JLabel(messages.getString("rows"));
                JLabel textBreadth = new JLabel(messages.getString("breadth"));
                JLabel textHeight = new JLabel(messages.getString("height"));
                
                NumberFormat integerNumberInstance = NumberFormat.getIntegerInstance();
                JFormattedTextField fieldColumn = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldRow = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldBreadth = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldHeight = new JFormattedTextField(integerNumberInstance);
                
//                JCheckBox checkScroll = new JCheckBox("Put into JScrollPane");
                JCheckBox checkWordWrap = new JCheckBox(messages.getString("useWordWrapOption"));
                
                // Set text field width
                final int textFieldWidth = 4;
                fieldColumn.setColumns(textFieldWidth);
                fieldRow.setColumns(textFieldWidth);
                fieldBreadth.setColumns(textFieldWidth);
                fieldHeight.setColumns(textFieldWidth);
                
                // Insert current object properties into text fields and checkboxes
                int selectedRow = table.getSelectedRow();
                fieldColumn.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getColumns()));
                fieldRow.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getRows()));
                fieldBreadth.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getBreadth()));
                fieldHeight.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getHeight()));
//                checkScroll.setSelected(tableModel.data.elementAt(selectedRow).getUseJScrollPane());
                checkWordWrap.setSelected(tableModel.data.elementAt(selectedRow).getUseWordWrap());
                
                topPanel.add(textHint, BorderLayout.CENTER);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 1;
                panel.add(textColumn, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 1;
                panel.add(fieldColumn, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 2;
                panel.add(textRow, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 2;
                panel.add(fieldRow, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 3;
                panel.add(textBreadth, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 3;
                panel.add(fieldBreadth, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 4;
                panel.add(textHeight, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 4;
                panel.add(fieldHeight, c);
                
                
                JButton okButton = new JButton("Ok");
//                lowPanel.add(checkScroll, BorderLayout.NORTH);
                lowPanel.add(checkWordWrap, BorderLayout.CENTER);
                lowPanel.add(okButton, BorderLayout.SOUTH);
                
                
                frame.getContentPane().add(topPanel, BorderLayout.NORTH);
                frame.getContentPane().add(panel, BorderLayout.CENTER);
                frame.getContentPane().add(lowPanel, BorderLayout.SOUTH);
                frame.setSize(320, 240);
                //frame.pack();
                frame.setLocationByPlatform(true);
                frame.setVisible(true);
                
                okButton.addActionListener((e)->{
                	int currentRow = table.getSelectedRow();
                	if (!fieldColumn.getText().equals("") || fieldColumn.getText().equals("0")) {
                		tableModel.data.elementAt(currentRow).setColumns(Integer.parseInt(fieldColumn.getText()));
					}
					if (!fieldRow.getText().equals("") || fieldRow.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setRows(Integer.parseInt(fieldRow.getText()));
					}
					if (!fieldBreadth.getText().equals("") || fieldBreadth.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setBreadth(Integer.parseInt(fieldBreadth.getText()));
					}
					if (!fieldHeight.getText().equals("") || fieldHeight.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setHeight(Integer.parseInt(fieldHeight.getText()));
					}
//					tableModel.data.elementAt(currentRow).setUseJScrollPane(checkScroll.isSelected());
					tableModel.data.elementAt(currentRow).setUseWordWrap(checkWordWrap.isSelected());
					tableModel.fireTableRowsUpdated(currentRow, currentRow);
					frame.dispose();
                });
            }
        });
    }
    
    /**
     * Will display a window with settings, specified for an object
     * of type JTextField.
     */
    public static void showJTextFieldSettings() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame(messages.getString("textFieldSettingsTitle"));
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                try {
                	UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception e) {
                   e.printStackTrace();
                }
                JPanel panel = new JPanel();
                JPanel topPanel = new JPanel();
                JPanel lowPanel = new JPanel();
                
                panel.setLayout(new GridBagLayout());
                topPanel.setLayout(new BorderLayout());
                lowPanel.setLayout(new BorderLayout());
                
                GridBagConstraints c = new GridBagConstraints();
                panel.setOpaque(true);
                JLabel textHint = new JLabel(messages.getString("textFieldSettingsInstruction"));
                JLabel textColumn = new JLabel(messages.getString("columns"));
                JLabel textRow = new JLabel(messages.getString("rows"));
                JLabel textBreadth = new JLabel(messages.getString("breadth"));
                JLabel textHeight = new JLabel(messages.getString("height"));
                
                NumberFormat integerNumberInstance = NumberFormat.getIntegerInstance();
                JFormattedTextField fieldColumn = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldRow = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldBreadth = new JFormattedTextField(integerNumberInstance);
                JFormattedTextField fieldHeight = new JFormattedTextField(integerNumberInstance);
                
                // Set text field width
                final int textFieldWidth = 4;
                fieldColumn.setColumns(textFieldWidth);
                fieldRow.setColumns(textFieldWidth);
                fieldBreadth.setColumns(textFieldWidth);
                fieldHeight.setColumns(textFieldWidth);
                
             // Insert current object properties into text field
                int selectedRow = table.getSelectedRow();
                fieldColumn.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getColumns()));
                fieldRow.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getRows()));
                fieldBreadth.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getBreadth()));
                fieldHeight.setText(String.valueOf(tableModel.data.elementAt(selectedRow).getHeight()));
                
                topPanel.add(textHint, BorderLayout.CENTER);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 1;
                panel.add(textColumn, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 1;
                panel.add(fieldColumn, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 2;
                panel.add(textRow, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 2;
                panel.add(fieldRow, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 3;
                panel.add(textBreadth, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 3;
                panel.add(fieldBreadth, c);
                
                c.anchor = GridBagConstraints.EAST;
                c.gridx = 0;
                c.gridy = 4;
                panel.add(textHeight, c);
                
                c.anchor = GridBagConstraints.WEST;
                c.gridx = 1;
                c.gridy = 4;
                panel.add(fieldHeight, c);
                
                frame.getContentPane().add(topPanel, BorderLayout.NORTH);
                frame.getContentPane().add(panel, BorderLayout.CENTER);
                frame.getContentPane().add(lowPanel, BorderLayout.SOUTH);
                frame.setSize(320, 240);
                //frame.pack();
                frame.setLocationByPlatform(true);
                frame.setVisible(true);
                
                JButton okButton = new JButton("Ok");
                lowPanel.add(okButton, BorderLayout.SOUTH);
                
                okButton.addActionListener((e)->{
                	int currentRow = table.getSelectedRow();
                	if (!fieldColumn.getText().equals("") || fieldColumn.getText().equals("0")) {
                		tableModel.data.elementAt(currentRow).setColumns(Integer.parseInt(fieldColumn.getText()));
					}
					if (!fieldRow.getText().equals("") || fieldRow.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setRows(Integer.parseInt(fieldRow.getText()));
					}
					if (!fieldBreadth.getText().equals("") || fieldBreadth.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setBreadth(Integer.parseInt(fieldBreadth.getText()));
					}
					if (!fieldHeight.getText().equals("") || fieldHeight.getText().equals("0")) {
						tableModel.data.elementAt(currentRow).setHeight(Integer.parseInt(fieldHeight.getText()));
					}
					tableModel.fireTableRowsUpdated(currentRow, currentRow);
					frame.dispose();
                });
            }
        });
    }
}
